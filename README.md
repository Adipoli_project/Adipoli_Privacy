# അടിപൊളി🕶Privacy
**സ്വകാര്യതയെ ബഹുമാനിക്കുന്ന സ്വതന്ത്ര സേവനങ്ങളുടെ ഒരു ചെറിയ പട്ടിക**
## 🧭വെബ് ബ്രൗസറുകൾ
#### ❌ഉപേക്ഷിക്കു
- ഗൂഗിൾ ക്രോം(Google Chrome)
#### ✔ഉപയോഗിക്കു
- ഫയർഫോക്സ് (Firefox)
- Tor ബ്രൌസർ
* ബ്രോമൈറ്റ് (Bromite)
* ഫയർഫോക്സ് (Firefox)
* Tor ബ്രൌസർ
## 💬മെസ്സേജിങ് അപ്ലിക്കേഷനുകൾ
#### ❌ഉപേക്ഷിക്കു
- വാട്സപ്പ് (Whatsapp)
- ഫേസ്ബുക്ക് മെസഞ്ചർ (Facebook Messenger)
#### ✔ഉപയോഗിക്കു
* [സിഗ്നൽ (Signal)](https://signal.org/ml/)
* [എലമെന്റ് (Element)](https://element.io/)
## ⌨ഓഫീസ് സോഫ്റ്റ്വെയർ
* ലീബർ ഓഫീസ്(LibreOffice)
* ഒൺലി ഓഫീസ്(Only Office)
## 🔑പാസ്‍വേഡ് മാനേജറുകൾ
- [Keepass XC](https://keepassxc.org/)
    - നിങ്ങളുടെ വിവരങ്ങൾ‌ സംഭരിക്കുകയും മാനേജുചെയ്യുകയും ചെയ്യുന്ന ഒരു ആധുനിക, സുരക്ഷിത, ഓപ്പൺ‌ സോഴ്‌സ് പാസ്‌വേഡ് മാനേജറാണ് കീപാസ് എക്സ് സി. ഇന്റർനെറ്റ് കണക്ഷൻ പോലും ആവശ്യമല്ല.
<br />**പിന്തുണയ്‌ക്കുന്ന പ്ലാറ്റ്ഫോമുകൾ:** [Windows](https://keepassxc.org/download/), [Linux](https://keepassxc.org/download/#linux), [MacOS](https://keepassxc.org/download/#mac)
<br />[സോഴ്സ് കോഡ്](https://github.com/keepassxreboot/keepassxc)
- [ബിറ്റ്വാർഡൻ (Bitwarden)](https://bitwarden.com)
    - ഒരു ക്ലൗഡ് ബെയ്‌സ്ഡ് ഓപ്പൺ സോഴ്സ് പാസ്‍വേഡ് മാനേജർ. ഒന്നിലധികം ഉപകരണങ്ങളിലുടനീളം പാസ്‌വേഡുകൾ സമന്വയിപ്പിക്കാൻ സഹായിക്കുന്നു.കൂടാതെ ഇത് സ്വയം ഹോസ്റ്റ് ഹോസ്റ്റുചെയ്യാനാകും. 
<br />**പിന്തുണയ്‌ക്കുന്ന പ്ലാറ്റ്ഫോമുകൾ:** Windows, Linux, MacOS, [Android](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden), [iOS](https://apps.apple.com/app/bitwarden-free-password-manager/id1137397744),[വെബ്](https://vault.bitwarden.com)
<br />[ഡൌൺലോഡ് ചെയ്യുക](https://bitwarden.com/download/)<br />[F-droid](https://mobileapp.bitwarden.com/fdroid/)
<br />[സോഴ്സ് കോഡ്](https://github.com/bitwarden)
## 📕ഡിജിറ്റൽ നോട്ട്ബുക്ക്
- [Joplin](https://joplinapp.org/)
## 🤝സോഷ്യൽ നെറ്റ്വർക്കുകൾ
- [Mastodon](https://joinmastodon.org/)
    - ട്വിറ്ററിന് പകരമായ വികേന്ദ്രീകൃത നെറ്റ്‌വർക്കാണ് മാസ്റ്റഡോൺ.
- [Pixelfed](https://pixelfed.org)
    - ചിത്രങ്ങൾ‌ പങ്കിടുന്നതിനുള്ള ഒരു ഫെഡറേറ്റഡ് പ്ലാറ്റ്‌ഫോമാണ് Pixelfed.

